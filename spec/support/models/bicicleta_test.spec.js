var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');


describe('Testing Bicicletas', function() {

    beforeAll(function(done){

        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost:27017/testdb';
            mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;

            db.on('error', console.error.bind(console, 'MongoDB conection error'));
            db.once('open', function() {

                console.log('We are coneected to test database');
                done();
            
                     
            });
        });
    });


    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        })
    })

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1])
    
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
            done();
        });
    });


// FALTA TERMINAR DE ARREGLAR ESTA PARTE DE LOS SPECT!!!!


    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', function (done) {
            Bicicleta.allBicis(function(err, bicis) {

                expect(bicis.length).toBe(0);

                done();
            })
        })
    })

    describe('Bicicleta.add', () => {
        it('Agrega una sola bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });


    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bici con code 1', (done)=> {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: 'verde', modelo:'urbana' });
                Bicicleta.add(aBici, function ( err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: 'roja' , modelo:'montaña'});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done()

                        })
                    })  
                })
            })
        })
    })



    describe('Bicicleta.removeByCode', () => {
        it('se debe eliminar ua bici y quedar en 0', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code:1, color: 'verde', modelo: 'urbana'});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    Bicicleta.removeByCode(1, function(error, targetBici){
                        expect(bicis.length).toBe(0);

                        done()
                    })
                })
            })
        })
    })
})




// ESTO PERTENECE AL LISTEN DEL CUERPO!



/*
describe('Bicicleta.createInstance', () => {
    it('Crea una instancia de bicicleta', () =>{
        var bici = Bicicleta.createInstance(1, "verde", "montain", [-34.5, -54.1]);

        expect(bici.code).toBe(1);
        expect(bici.color).toBe("verde");
        expect(bici.modelo).toBe("montain");
        expect(bici.ubicacion[0]).toEqual(-34.5);
        expect(bici.ubicacion[1]).toEqual(-54.1);
    });
});*/




/*
beforeEach(() => { Bicicleta.allBicis = [];
});
describe('Bicicleta.add' , () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-33.5954807,-70.6318299]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});


beforeEach(() => { Bicicleta.allBicis = [];
});
describe('Bicicleta.findById', () => {
    it('De devolver la bici con ID 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'verde', 'mountain');
        var aBici2 = new Bicicleta(2, 'café', 'urbana');
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);


    });
});

beforeEach(() => { Bicicleta.allBicis = [];
});
describe('Bicicleta.removeById', () => {
    it('De eliminar una bici por el ID', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1, 'verde', 'mountain');
        var aBici2 = new Bicicleta(2, 'café', 'urbana');
        var aBici3 = new Bicicleta(3, 'naranjo', 'carrera');
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);
        Bicicleta.add(aBici3);

        var targetBici = Bicicleta.removeById(3);

        expect(targetBici).toBe(undefined);
        expect(Bicicleta.allBicis.length).toBe(2);


    });
});*/